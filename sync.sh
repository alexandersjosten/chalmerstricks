RHOST=hamide[CHANGEME]@remote11.chalmers.se
RPATH=/chalmers/groups/edu2009/www/www.cse.chalmers.se/year/2016/course/TDA352_Cryptography[CHANGEME]
rsync -priv --delete --chmod=F664,D775 --checksum $* \
  website-folder-in-local-machine[CHANGEME]/ \
  ${RHOST}:${RPATH}
