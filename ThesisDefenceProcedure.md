(extracted from [Insidan
CSE](https://www.chalmers.se/insidan/sites/cse/doctoral-studies/thesisdefence)
and [Rules of
Procedure-Doctoral-Programmes-2015-03-03-2](RulesofProcedure-Doctoral-Programmes-2015-03-03-2.pdf)
)

Check list for PhD dissertation planning 
========================================
## ** Print this page and fill in the gap and check off items as you progress. **


Discuss and get approval in a follow-up meeting.

Make sure you have passed all the needed courses ( 60
credits including 15 GTS credits, GTS activities mandatory to all
doctoral students at Chalmers enrolled after 1 September 2012)


[TranscriptOfRecord](https://student.portal.chalmers.se/doctoralportal/services/Pages/TranscriptOfRecord_Doctoral.aspx)



-   Introductory day mandatory to PhD students GFOK015 - 0 HEC
-   Career Planning - Your Personal Leadership GFOK010 - 1,5 HEC
-   Teaching, Learning and Evaluation GFOK020 - 3 HEC
-   Sustainable Development: Values, Technology in Society, and the
    Researcher GFOK10 - 3 HEC
-   Popular Science Presentation mandatory with the GTS development
    program GFOK070 : Scheduled for 14 December

[The popular science summary](https://student.portal.chalmers.se/doctoralportal/gts/Popular%20Science%20Presentation/Pages/Written-.aspx)


Find opponents and schedule your defense. Acceptable periods are 25
August-21 December or 9 January-12 June.

3-5 grading committees has to be appointed.

-   Faculty Opponent: -------------
-    Grading committee: ------------- , -------------, -------------
-    Reserve standby committee member: -------------
-    Defence Chairman and Formal Examiner: -------------

4-5 month before the defense notify the Head of Department and related
people on cc. Here: [vice head of department at CSE, Agneta
Nilsson](vprefphd.cse@chalmers.se)

-   Name of student and link to homepage.
-   Title of the PhD thesis.
-   Supervisor.
-   Co-supervisor.
-   Examiner.
-   Suggested date and time. Venue for the defense.

(3 months before the defense) Send a preliminary version of the thesis
to your opponent and the graduate committee.

Make sure that the date is added to the department calendar. (apparently
two defenses in one day are not acceptable)

Defence date : -----------
-------------------------- You may ask them to book a week and then decide about the exact day later.


[Book a room](salsbokningen@chalmers.se) as soon as  the date is confirmed.

-------------- (2 months before the defense) Send a written statements
to the [Vice Head of Department at CSE, Agneta Nilsson](vprefphd.cse@chalmers.se). (specify that recipient has received
the thesis and accept it for public defense otherwise their
respond/comment). Then the date, time and venue are confirmed.

-------------- (1-2 months before the defense)Printing and distributing



[Ask for a report number](eva.axelsson@chalmers.se) for the reverse of
the title page: 153D

[ISBN and serial sequence
number](https://research.chalmers.se/en/publication/isbn) Your doctoral
dissertation Dynamic Enforcement of Differential Privacy has been
assigned the following numbers: ISBN: 978-91-7597-685-3 Series number:
4366 in the series Doktorsavhandlingar vid Chalmers tekniska högskola.
Ny serie (ISSN 0346-718X)

[Layout](https://student.portal.chalmers.se/doctoralportal/handbook/Doctoral%20Thesis%20Defence/Pages/Layout-of-the-thesis.aspx)

[Distribution of the
thesis](https://student.portal.chalmers.se/doctoralportal/handbook/Doctoral%20Thesis%20Defence/Pages/Distribution-of-the-thesis.aspx)

-   Faculty opponent 1
-   Members of the evaluation committee 1 to each member
-   Department expedition 1-10
-   The main library or the Lindholmen library 11 + 11 defence title
    sheet2
-   The Office of Admissions and Degrees 1 (The doctoral thesis signed
    by Deputy Head of Department or Head of Department should be sent to
    the Office of Admissions and Degrees by internal post or handed in
    to the Office of Degrees mailbox in the entrance at Sven Hultins 
    gata 8)
-   Departments outside Chalmers 5-10

personally hand one copy of the thesis to the head of department

send the title sheet of the thesis, with the abstract, by e-mail to all
members of the CSE department council.

-------------- (4 weeks before the defense) Public announcement of the
thesis

-   [Chalmers Publication
    Library (CPL)](https://student.portal.chalmers.se/doctoralportal/handbook/Doctoral%20Thesis%20Defence/Pages/Printing-of-the-thesis.aspx)
    and
    [here](https://research.chalmers.se/en/myresearch/?tab=publication-tab)
-   Send an e-mail to the [department communications
    officer](info.cse@chalmers.se) for announcement in the web calendar
    and Weekly News.

[Public defense](https://student.portal.chalmers.se/doctoralportal/handbook/Doctoral%20Thesis%20Defence/Pages/Defence-and-conferment-ceremony.aspx)

[Party, book the lunch room
(rules)](https://www.chalmers.se/insidan/sites/cse/verksamhetsstod/routines-processes/how-to-book-rooms/)

(After the defense) [apply for the degree
certificate](examen.stodet@chalmers.se) (takes \~ 2 months)


![GTS overview](GTS_oversikt_en.png)